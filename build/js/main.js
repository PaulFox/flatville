$(function() {
    $('.dropdown-menu-toggle').on('click', function () {
        var $thisMenu = $(this).parent('.dropdown-menu');
        if ($thisMenu.hasClass('opened')) {
            $thisMenu.removeClass('opened');
        } else {
            $('.dropdown-menu-toggle').parent('.dropdown-menu').removeClass('opened');
            $thisMenu.addClass('opened');
        }
    });

    $(document).on('click', '.nav-list-menu-toggler', function(e) {
        e.preventDefault();
        $navContainer = $(this).next('.nav-list__menu');
        $navContainer.toggleClass('expanded');
        $navContainer.find('.nav-list__item:not(.active)').slideToggle();
    });

    $(document).on('click', '.show-phone button', function(e) {
        e.preventDefault();
        $(this).hide();
        $(this).next().show();
    });

    $(document).on('click', '.btn-show-form', function(e) {
        e.preventDefault();
        $($(this).attr('data-target-form')).slideToggle(500);
    });

    $('.select-item .custom-select select').on('click', function() {
        if($(this).val()) {
            $(this).parent().next('.btn').addClass('active');
        }
    });

    function swiperBreakpoints(pageHasAside) {
        var sliderWithAside = {
            365: {
                coverflowEffect: {
                    rotate: 0,
                    stretch: 94,
                    depth: 60,
                    modifier: 3,
                    slideShadows : 0,
                }
            },
            767: {
                coverflowEffect: {
                    rotate: 0,
                    stretch: 110,
                    depth: 80,
                    modifier: 3,
                    slideShadows : 0,
                }
            },
            1200: {
                coverflowEffect: {
                    rotate: 0,
                    stretch: 250,
                    depth: 110,
                    modifier: 3,
                    slideShadows : 0,
                }
            },
            1520: {
                coverflowEffect: {
                    rotate: 0,
                    stretch: 186,
                    depth: 90,
                    modifier: 3,
                    slideShadows : 0,
                }
            }
        };

        var sliderWithoutAside = {
            365: {
                coverflowEffect: {
                    rotate: 0,
                    stretch: 94,
                    depth: 60,
                    modifier: 3,
                    slideShadows : 0,
                }
            },
            767: {
                coverflowEffect: {
                    rotate: 0,
                    stretch: 110,
                    depth: 80,
                    modifier: 3,
                    slideShadows : 0,
                }
            },
            1200: {
                coverflowEffect: {
                    rotate: 0,
                    stretch: 250,
                    depth: 110,
                    modifier: 3,
                    slideShadows : 0,
                }
            },
            1520: {
                coverflowEffect: {
                    rotate: 0,
                    stretch: 235,
                    depth: 120,
                    modifier: 3,
                    slideShadows : 0,
                }
            }
        };
        if (pageHasAside === 'true') {
            console.log('has aside');
            return sliderWithAside;
        } else if (pageHasAside === 'false') {
            return sliderWithoutAside;
        }
    }
    
    function swiperCoverflow(pageHasAside) {
        if (pageHasAside === 'true') {
            return {
                rotate: 0,
                stretch: 180,
                depth: 90,
                modifier: 3,
                slideShadows : 0,
            };
        } else if (pageHasAside === 'false') {
            return {
                rotate: 0,
                stretch: 220,
                depth: 78,
                modifier: 3,
                slideShadows : 0,
            };
        }

    }

    $('.flat-gallery-brief').each(function(index, item) {
        var swiper = new Swiper(item, {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 1,
            loop: true,
            navigation: {
                nextEl: $(item).parent().find('.flat-gallery-brief-next')[0],
                prevEl: $(item).parent().find('.flat-gallery-brief-prev')[0]
            },
            coverflowEffect: {
                rotate: 0,
                stretch: 142,
                depth: 70,
                modifier: 3,
                slideShadows : 0,
            },
            breakpoints: {
                365: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 72,
                        depth: 64,
                        modifier: 3,
                        slideShadows : 0,
                    }
                },
                767: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 85,
                        depth: 70,
                        modifier: 3,
                        slideShadows : 0,
                    }
                },
                1200: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 89,
                        depth: 74,
                        modifier: 3,
                        slideShadows : 0,
                    }
                },
                1520: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 114,
                        depth: 78,
                        modifier: 3,
                        slideShadows : 0,
                    }
                },
            }
        });
    });

    $('.flat-gallery-swiper').each(function(index, item) {
        var pageHasAside = $(item).attr('data-page-has-aside');
        var swiper = new Swiper(item, {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 1,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: '.flat-gallery-next',
                prevEl: '.flat-gallery-prev'
            },
            coverflowEffect: swiperCoverflow(pageHasAside),
            breakpoints: swiperBreakpoints(pageHasAside)
        });
    });

    $('.flat-gallery-small').each(function(index, item) {
        var swiper = new Swiper(item, {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 1,
            loop: true,
            navigation: {
                nextEl: $(item).parent().find('.flat-gallery-small-next')[0],
                prevEl: $(item).parent().find('.flat-gallery-small-prev')[0]
            },
            coverflowEffect: {
                rotate: 0,
                stretch: 124,
                depth: 80,
                modifier: 3,
                slideShadows : 0,
            },
            breakpoints: {
                365: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 94,
                        depth: 60,
                        modifier: 3,
                        slideShadows : 0,
                    }
                },
                767: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 110,
                        depth: 80,
                        modifier: 3,
                        slideShadows : 0,
                    }
                },
                992: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 103,
                        depth: 80,
                        modifier: 3,
                        slideShadows : 0,
                    }
                },
                1200: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 105,
                        depth: 80,
                        modifier: 3,
                        slideShadows : 0,
                    }
                },
                1520: {
                    coverflowEffect: {
                        rotate: 0,
                        stretch: 97,
                        depth: 80,
                        modifier: 3,
                        slideShadows : 0,
                    }
                }
            }
        });
    });
    
    // $('.flat-gallery-small').each(function(index, item) {
    //     new Swiper(item, {
    //         slidesPerView: 1,
    //         spaceBetween: 10,
    //         navigation: {
    //             nextEl: $(item).parent().find('.flat-gallery-small-next')[0],
    //             prevEl: $(item).parent().find('.flat-gallery-small-prev')[0]
    //         },
    //         thumbs: {
    //             swiper: new Swiper('#' + $(item).attr('data-thumbs'), {
    //                 slidesPerView: 2,
    //                 spaceBetween: 8,
    //                 freeMode: true,
    //                 watchSlidesVisibility: true,
    //                 watchSlidesProgress: true
    //             })
    //         },
    //     });
    // });
});