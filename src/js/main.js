import './vendor';

$('.dropdown-menu-toggle').on('click', function() {
    var $thisMenu = $(this).parent('.dropdown-menu');
    if($thisMenu.hasClass('opened')) {
        $thisMenu.removeClass('opened');
    } else {
        $('.dropdown-menu-toggle').parent('.dropdown-menu').removeClass('opened');
        $thisMenu.addClass('opened');
    }
});